\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{bookman}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage[hidelinks,pdftex,
			pdfauthor={Alfonso Landín Piñeiro},
			pdftitle={RIWS - Practica RI},
			pdfproducer={LaTeX},
			pdfcreator={pdflatex}]{hyperref}
\usepackage[scaled=1.05]{inconsolata}
\usepackage{listings}

\setlength{\parskip}{4pt}

\usepackage[left=2.50cm, right=2.50cm, top=3.00cm, bottom=3.00cm]{geometry}

\lstset{
	basicstyle=\ttfamily,
	columns=flexible}

%\usepackage{mathpazo}

\author{Alfonso Landín Piñeiro}
\title{%
	\textbf{Recuperación de Información y Web Semántica}\\
	\large Práctica RI}

\begin{document}
	\maketitle
	
	\section{Introducción}
	Para la realización de esta práctica se ha escogido el \textit{crawlear} e indexar la página del \textit{xestor de proxectos}\footnote{\url{https://xestor.fic.udc.es/xestor/}} de la Facultad de Informática de la Universidad de La Coruña. Esta página contiene la información de los proyectos de fin de carrera de las antiguas titulaciones impartidas en la facultad, los trabajos de fin de grado realizados en la titulación actual de Grado en Ingeniería Informática y los trabajos de fin de máster del Máster Universitario en Ingeniería Informática.
	
	El contenido de esta página no está indexado por ningún motor de búsqueda habitual ya que es necesario disponer de las credenciales de acceso adecuadas para acceder al mismo. Por otro lado las capacidades de búsqueda proporcionadas por la página son bastante limitadas, permitiendo realizar búsquedas por autor, director, título y estado. Es posible combinar estas búsquedas, pero no se pueden realizar búsquedas por el resto de campos disponibles con la información del proyecto, como pueden ser el resumen, la descripción, la metodología o las palabras clave, por citar unos cuantos. Tampoco es posible combinar búsquedas en el campo de director, para buscar los proyectos en los que dos personas han sido codirectores.
	
	Es por esto por lo que se considera algo útil el indexar el contenido de esta página, permitiendo realizar búsquedas más complejas sobre el contenido de la misma.
	
	La estructura de esta breve memoria se ha dividido en las siguientes secciones:
	\begin{itemize}[topsep=\parsep, itemsep=\parsep]
		\item \textbf{Introducción}: esta sección donde se explica el objetivo del trabajo.
		
		\item \textbf{Xestor de Proxectos}: en esta sección se explica la estructura de la página, así como las idiosincrasias de la misma.
		
		\item \textbf{Tecnologías}: donde se describen las tecnologías utilizadas y la motivación para el uso de las mismas.
		
		\item \textbf{Desarrollo del proyecto}: donde se explica como se ha realizado el mismo.
		
		\item \textbf{Manual de usuario}: donde se indican los pasos necesarios para la ejecución de las aplicaciones empaquetadas, su configuración y la compilación de estas desde el código fuente.
		
		\item \textbf{Funcionalidades implementadas}: donde se listan brevemente las mismas.
		
		\item \textbf{Trabajo futuro}: donde se nombran algunas posibles ampliaciones del trabajo realizado.
	\end{itemize}
	
	\section{Xestor de Proxectos}
	El contenido que se desea obtener para indexar de esta página es aquel contenido en las descripción de las instancias, termino utilizado en la página para referirse a los proyectos y trabajos (usándose el término proyecto para referirse a los anteproyectos). Para poder descubrir que instancias existen es necesario obtener el contenido de la lista de instancias, que se encuentra disponible de forma paginada.
	
	La primera inspección que se realizó es la de comprobar si existe algún archivo \textit{robots.txt} que indique si la página sugiere alguna restricción a los \textit{crawlers} que deseen indexarla. Peticiones tanto las URLs \url{https://xestor.fic.udc.es/robots.txt} y \url{https://xestor.fic.udc.es/proxectos/robots.txt} dio como resultado un \texttt{404 Not Found}.
	
	Para acceder a los contenidos de la página es necesario primero obtener una \textit{cookie} de sesión. Para esto hay que introducir las credenciales de acceso en la dirección \url{https://xestor.fic.udc.es/proyectos/user/login}. Algunas de las páginas están disponibles sin autenticación, pero la información mostrada en estas no es la misma que para un usuario identificado.
	
	La lista de instancias existentes se encuentra en la página \url{https://xestor.fic.udc.es/proyectos/user/consultarinstancia/false}. Esta lista es mostrada de forma paginada, cargándose el contenido de las distintas páginas de forma asíncrona cuando se selecciona alguno de los números del paginador. La información de estos nuevos resúmenes de instancias a mostrar está contenido en un archivo de tipo JSON. De estos resúmenes es posible obtener un enlace a la página con la información de cada instancia.
	
	Finalmente la información de cada instancia se encuentra disponible en la página que se obtiene al cargar la URL obtenida de la información del paginador. La respuesta a esta petición es siempre del tipo \texttt{302 Found} redirigiendo la petición a otra página. Es esta segunda dirección la mostrada en la barra de direcciones del navegador. Estas dos URLs tienen siempre el mismo patrón donde lo único que varía es el número de identificación de la instancia. La secuencia, para la instancia número 3991, es:
	\begin{itemize}[topsep=0pt, noitemsep]
		\item \url{ https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/3991}
		\item \url{https://xestor.fic.udc.es/proyectos/user/detalleinstancia/3991}
	\end{itemize}
	
	La forma de manejar el servidor estas peticiones es algo peculiar. La respuesta a las peticiones a la segunda URL (detalleinstancia) dependen únicamente de la última petición realizada a la primera URL (consultarinstancia), independientemente del id indicado en la URL de la petición. Por ejemplo si se realizan peticiones a las siguientes direcciones, en el mismo orden,
	\begin{itemize}[topsep=0pt, noitemsep]
		\item \url{https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/3991}
		\item \url{https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/211}
		\item \url{https://xestor.fic.udc.es/proyectos/user/detalleinstancia/3991}
	\end{itemize}
	\noindent se obtienen los detalles de la instancia 211 en la última petición. Esto hace que sea necesario controlar que las peticiones correspondientes a la obtención de los detalles de una instancia en particular se realicen de manera consecutiva, sin que se realice ninguna otra petición en el medio.
	
	El contenido de las páginas de información de las instancias tiene un formato estándar, con una clara separación entre los distintos campos con al información. Esto hace que la extracción del contenido no presente un reto especialmente difícil. Es necesario aún así tener en cuenta que no todos los campos se encuentran disponibles en todas las instancias.
	
	\section{Tecnologías}
	Dado que el sitio que se está \textit{crawleando} se encuentra detrás de una acceso con credenciales y que la idiosincrasia del mismo es bastante peculiar se ha decidido implementar un \textit{crawler} sencillo en vez de utilizar alguno de los que hay disponibles.
	
	Para la implementación del \textit{crawler} se ha usado el conjunto de librerías Akka, en particular su versión en Scala. Para la indexación y la búsqueda se ha utilizado Elasticsearch. Para la implementación de la vista de acceso a la búsqueda se ha usado Searchkit, un conjunto de componentes React para la búsqueda en Elasticsearch.
	
	\subsubsection*{Scala\footnote{\url{http://scala-lang.org/}}}
	Scala es un lenguaje funcional orientado a objetos. La principal implementación de Scala corre sobre la máquina virtual de Java, JVM, y es posible acceder a librerías de Java desde el código de Scala. Existen ademas implementaciones que producen código en Javascript y nativo, aunque estas versiones no soportan todas las funcionalidades del lenguaje y/o todas las clases de la librería estándar.
	
	\subsubsection*{Akka\footnote{\url{https://akka.io/}}}
	Akka son un conjunto de librerías, entre las que se incluye una implementación del \textit{actor model}\footnote{\url{https://en.wikipedia.org/wiki/Actor_model}}, \texttt{akka-core}, sobre las que se basan una librería de cliente y servidor HTTP, \texttt{akka-http}, y una implementación de \textit{reactive streams}\footnote{\url{http://www.reactive-streams.org/}}, \texttt{akka-streams}, que se han utilizado para la realización de la practica.
	
	Se ha usado esta librería porque facilita la implementación de programas que se puedan expresar como la realización de una serie de pasos de forma secuencial de manera repetida, facilitando la concurrencia de las distintas etapas. Aunque para la realización de esta práctica no es necesario la concurrencia, dado que se está realizado el \textit{crawling} de un único sitio Web, se ha escogido esta librería como un ejercicio didáctico.
	
	\subsubsection*{Elasticsearch%
		\footnote{\url{https://www.elastic.co/products/elasticsearch}}}
	Elasticsearch es un motor de indexación y búsqueda de con capacidades para la agregación y analítica de datos. Este motor usa Apache Lucene\footnote{\url{http://lucene.apache.org/index.html}} para indexar los documentos, proporcionando capacidades de distribución y replicación de los índices sobre este de forma trasparente, además de proporcionar un API REST para acceder a las características proporcionadas.
	
	La versión usada para la realización de esta práctica es la 5.6.3. Durante la realización de la misma una nueva versión, la 6.0, fue lanzada. Se descartó el usar esta versión dado que los \textit{bindings} de Scala usados para la construcción de las peticiones no tenían disponible una versión final con soporte para dicha versión de Elasticsearch.
	
	\subsubsection*{Searchkit\footnote{\url{http://www.searchkit.co/}}}
	Searchkit es una colección de componentes de interfaz de usuario de React\footnote{\url{https://reactjs.org/}}. Estos se encargan de comunicarse directamente con el cluster de Elasticsearch o a través de un proxy y mostrar los resultados de busqueda en el navegador. Los autores de estos componentes proporcionan también una implementación un proxy sencillo para el acceso a Elasticsearch que limita el acceso a solo el \textit{endpoint} \texttt{\_search} y que permite especificar una función para filtrar la petición antes de ser enviada al cluster.
	
	Tanto este proxy como Searchkit, al ser componentes React, se encuentran implementados sobre Node.js\footnote{\url{https://nodejs.org/}}.
	
	\section{Desarrollo del proyecto}
	El desarrollo del proyecto se ha dividido en tres fases: estudio del problema, donde se ha comprobado la viabilidad del mismo y los requisitos necesarios para llevarlo a cabo, implementación del \textit{crawler} e implementación de la vista.

	\subsection{Estudio del problema}
	El primer trabajo consistió en el estudio de la viabilidad del proyecto. Para esto se comprobó si era posible las descarga de distintas páginas del sitio a tratar. Durante este se comprobó que era necesario autenticarse en el sitio para obtener una \textit{cookie} de sesión con la que poder acceder al resto del contenido.

	La obtención de de esta \textit{cookie} es posible replicando el comportamiento estándar del navegador al acceder a la página, descargando la página de autenticación, extrayendo de esta la información necesaria del formulario para realizar el \textit{logeo}, y realizando una petición POST con las credenciales, guardando la \textit{cookie} obtenida para la realización del resto de operaciones.
	 
	Para el descubrimiento de instancias se comprobó si era posible descargar la información del paginador. Esta información se descarga de manera asíncrona al seleccionar el usuario una de las páginas, con excepción de la primera página, cuya información está ya contenida dentro de la petición inicial a la página que lista las instancias disponibles. Esta petición asíncrona se realiza mediante una petición POST con siempre los mismos contenidos en el cuerpo y solo cambiando la \textit{id} de instancia usada en la dirección de la petición. Es necesario incluir la cabecera \texttt{X-Requested-With: XMLHttpRequest} para que el servidor responda a la petición.
	 
	El contenido de la respuesta a esta petición en un archivo de tipo JSON, que contiene en uno de los campos en su interior el HTML de la nueva hoja del paginador a mostrar. Es este contenido el que se ha de analizar para descubrir que instancias hay disponibles en la página. Esta respuesta también contiene los enlaces para el selector de hoja en la paginación. Entre ellos se encuentra un enlace a la última página por lo que también se puede usar este archivo para descubrir cuantas hojas en total hay en el paginador.
	 
	Finalmente se comprobó que era posible descargar las propias páginas con la información de las instancias y que se podía extraer la información contenida en las mismas de una forma consistente.
	 
	\subsection{Implementación del \textit{crawler}}
	El funcionamiento del \textit{crawler} pasa por dos fases: arranque y \textit{crawling}. En la primera fase se carga la configuración del \textit{crawler} y se crean las estructuras necesarias para la fase siguiente, y en la segunda fase se realiza el \textit{crawling} en si, descargando, analizando e indexando las instancias.
	 
	En el arranque del \textit{crawler} hay que distinguir dos posibilidades: que sea la primera vez que se arranca o que haya un estado previo guardado. En cualquiera de los dos casos como parte del proceso de arranque es necesario preguntar al usuario por los datos de sus credenciales y obtener una \textit{cookie} de sesión para la realización del resto de peticiones.
	 
	Si es la primera vez que se está arrancando el \textit{crawler} es necesario crear el índice con el \textit{mapping} correspondiente al tipo de documento que se quiere indexar. Este \textit{mapping} contiene información sobre los distintos campos del documento y como se han de indexar estos, indicando el tipo de los campos y como se ha de analizar el contenido de los mismo por parte de Elasticsearch/Lucene. Es también necesario en el arranque en frio el obtener una frontera inicial de las instancias a descargar así como un listado de las páginas del paginador disponibles para el descubrimiento de las instancias.
	 
	Si al arrancar se detecta que existe un estado previo guardado es necesario cargar el mismo. Este estado contiene un listado de las instancias conocidas y cuales de estas han sido descargados, así como un listado de las páginas del paginador que se han descargado y el número de páginas conocidas del mismo. Durante este arranque se realiza una comprobación de si ha aumentado el número de páginas disponibles en el paginador desde que el \textit{crawler} se ejecutó por última vez.
	 
	Todo el proceso de arranque y obtención de los datos necesarios para la realización del \textit{crawling} se ha encapsulado dentro de un actor del tipo FSM (\textit{finite state machine} o máquina de estados finitos) \cite{akka-fsm}. Este tipo de actores está basado en el patrón de diseño del mismo nombre de Erlang/OTP \cite{erlang-fsm}. En la Figura \ref{fig:bootup} se puede observar un diagrama de estados UML indicando los distintos estados por los que pasa el proceso de arranque así como los eventos que provocan las transiciones y la acciones realizadas al ocurrir estas.
	 
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{img/state.pdf}
		\caption{Diagrama de estados del arranque del \textit{crawler}}
		\label{fig:bootup}
	\end{figure}
	
	Una vez se ha realizado el proceso de arranque se procede a construir el \textit{pipeline} de los componentes que realizaran el proceso de \textit{crawling}. Existe un fichero de configuración que indica varias opciones que se han de tener en cuenta durante este proceso. Entre ellas se cuentan las opciones de indexar o no el contenido descargado y la de guardar o no este mismo contenido a disco. Esto hace variar la estructura final del \textit{pipeline}.
	
	Los componentes implementados realizan las diversas tareas necesarias durante el proceso de \textit{crawling}, como son el descargar las páginas con la información de las instancias, analizar el contenido de las mismas o indexar los documentos en Elasticsearch. En la Figura \ref{fig:flow} se puede observar la estructura del \textit{pipeline} así como la naturaleza de los datos que fluyen a través del mismo en sus distintas etapas.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{img/crawler-flow.pdf}
		\caption{Estructura del \textit{pipeline} de \textit{crawling} en el caso de que se desee indexar y guardar los documentos a disco a la vez}
		\label{fig:flow}
	\end{figure}
	
	Además de este flujo existe también el actor encargado de controlar el proceso de \textit{crawling}. Este actor es notificado por parte de algunos componentes de eventos que ocurren durante el proceso. 
	
	Uno de estos eventos ocurre cuando se descarga una página del paginador, lo que supone el descubrimiento de nuevas instancias. Estas instancias son añadida a la frontera como parte del funcionamiento normal de \textit{pipeline}. Esta notificación es necesaria para que se guarden estas nuevas instancias descubiertas en la lista de instancias conocidas que serán cargadas en las siguientes ejecuciones del crawler.
	
	Otro de los eventos de los que el actor principal es notificado ocurre cuando una instancia has sido tanto indexada como guardada a disco. Si solo se está realizando una de estas dos acciones, porque así se ha indicado en la configuración, este evento se disparará cuando se complete esta. Si se están realizando ambas acciones el evento se emitirá cuando ambas hayan sido realizadas. El actor principal se encargará de guardar dicha instancia en la lista de instancias descargadas, igualmente que en el caso anterior, para poder conocer el estado del \textit{crawling} en siguientes ejecuciones del mismo.
	
	\subsection{Implementación de la vista}
	Para la realización de la vista de búsqueda se ha optado por usar un conjunto de componentes React para la realización de búsquedas en índices de Elasticsearch y la visualización de los resultados de las mismas llamado Searchkit. Además de esta funcionalidad este \textit{toolkit} permite la realización de filtrado de la búsqueda por facetas o la sugerencia de términos de búsqueda si los términos actuales han dado como resultado una lista vacía, entre otras funcionalidades.
	
	La implementación se ha realizado partiendo de un proyecto básico\footnote{\url{https://github.com/searchkit/searchkit-starter-app}}. A este proyecto se le ha añadido un proxy para la realización de las búsquedas, para que la vista no acceda directamente a la instancia de Elasticsearch. Este proxy se ha implementado usando Searckit-express\footnote{\url{https://github.com/searchkit/searchkit-express}}. Este módulo limita el acceso al índice permitiendo solo peticiones al \textit{endpoint} \texttt{\_search}. Es posible ademas realizar un procesado de las consultas entrantes para filtrar peticiones no deseadas, aunque no se ha hecho uso de esta funcionalidad.
	
	Además de la búsqueda por palabras se han añadido facetas para filtrar los resultados por titulación, director y/o tutor. Los resultados de la búsqueda muestran el título del proyecto, el autor y los directores así como el campo resumen si este está disponible, mostrando el campo descripción en caso de que este no exista. Se ha usado un componente extra de React, react-show-more\footnote{\url{https://github.com/One-com/react-show-more}}, para mostrar este campo de forma más breve, permitiendo expandirlo para mostrar todo su contenido. Los resultados de busqueda además enlazan a la página correspondiente de la instancia en el Xestor.
	
	\section{Manual de usuario}
	En esta sección se detallan los pasos necesarios para la ejecución de las aplicaciones ya empaquetadas, seguido de una descripción más detallada de los pasos necesarios para compilar y ejecutar las aplicaciones a partir del código fuente así como los parámetros de configuración de los distintos componentes.

	La ejecución de las aplicaciones presupone que hay una instancia de Elasticsearch corriendo. Las configuraciones por defecto presuponen que esta instancia se encuentra corriendo en la máquina local, escuchando en el puerto 9200. Es posible configurar la localización de esta instancia en un archivo de configuración en caso del \textit{crawler}, siendo necesario usar una variable de entorno en el caso del proxy. La vista presupone que hay una instancia del proxy corriendo en la máquina local en el puerto 4000. Para cambiar este parámetro es necesario realizar el cambio en el código fuente y reconstruir la aplicación de nuevo.
	
	\subsection{Aplicaciones empaquetadas}
	Se incluyen tres aplicaciones empaquetadas: el \textit{crawler}, el proxy acceso a Elasticsearch y la aplicación React de la vista ya empaquetada para ser servida desde cualquier servidor HTTP de forma estática.
	
	Para correr el \textit{crawler} es necesario que esté instalada una máquina virtual de Java, versión 8 o superior. Para ejecutar la aplicación hay que ejecutar el comando \texttt{riws-crawler} que se encuentra en el directorio \texttt{bin}. Por ejemplo, estando en el directorio raíz de la aplicación:
	\begin{lstlisting}
	$ bin/riws-crawler
	\end{lstlisting}
	
	La configuración del \textit{crawler} se puede modificar en el archivo \texttt{conf/crawler.conf}. Las distintas opciones se detallan en la sección posterior correspondiente a la compilación del mismo.
	
	Para correr el proxy es necesario tener instalado node.js. Una vez descomprimido es necesario ejecutar una primera vez el comando:
	\begin{lstlisting}
	$ npm install
	\end{lstlisting}
	
	Una vez ejecutado este comando se puede ejecutar el proxy con el comando:
	\begin{lstlisting}
	$ node server.js
	\end{lstlisting}
	
	Para ejecutar la vista es necesario disponer de un navegador y un servidor web que sirva los archivos del mismo. Es posible usar un simple servidor en Python para servir los archivos contenidos en la carpeta donde se ejecute uno de los siguientes comandos:
	\begin{lstlisting}
	$ python2 -m SimpleHTTPServer 8000
	$ python3 -m http.server 8000
	\end{lstlisting}
	
	\subsection{Crawler}
	El \textit{crawler} se encuentra en la carpeta \texttt{riws-crawler} del archivo entregado. La aplicación se ha de compilar y empaquetar usando la herramienta estándar de Scala para el manejo de dependencias y compilación, SBT\footnote{\url{http://www.scala-sbt.org/}}. Ademas de compilar y crear un archivo JAR con las clases del proyecto se ha configurado el plugin de SBT sbt-native-packager\footnote{\url{https://github.com/sbt/sbt-native-packager}}. Es posible crear un archivo comprimido con este plugin con la aplicación y sus dependencias, así como un \textit{script} para la ejecución de la misma. De esta manera la única dependencia que se ha de satisfacer para ejecutar la aplicación empaquetada es disponer de una máquina virtual de Java debidamente configurada, de una versión 8 o superior.
	
	\subsubsection*{Compilación, empaquetamiento y ejecución de la aplicación}
	Para compilar la aplicación es necesario ejecutar el comando:
	\begin{lstlisting}
	$ sbt compile
	\end{lstlisting}
	
	Es posible ejecutar la aplicación usando SBT. En este caso la aplicación desconoce cual es su directorio de ejecución, por lo que usa la configuración por defecto para este caso que es guardar el estado del \textit{crawling} y las instancias descargadas en \texttt{/tmp/riws-crawler}. Para ejecutar la aplicación usando SBT es necesario introducir el comando:
	\begin{lstlisting}
	$ sbt run
	\end{lstlisting}
	
	Si se desea crear un archivo comprimido con la aplicación empaquetada hay que ejecutar uno de los siguiente comandos. El archivo empaquetado sera depositado en el directorio \texttt{target/universal}. Para ejecutar la aplicación hay que extraer los contenidos de este archivo y ejecutar el archivo \texttt{bin/riws-crawler} dentro del directorio de la aplicación.
	\begin{lstlisting}
	$ sbt packageZipTarball	    # Crea un archivo .tgz.
	$ sbt packageXzTarball	    # Crea un archivo .txz.
	\end{lstlisting}
	
	Es posible crear los contenidos de este ejecutable sin comprimirlos, creando la estructura de la aplicación en \texttt{target/universal/stage}. La aplicación es este directorio se comportará al ser ejecutada de manera similar a como lo haría al descomprimir uno de los ficheros generados con los comandos anteriores en cualquier otro lugar. Es posible cambiar la configuración de la aplicación editando el archivo \texttt{conf/crawler.conf} que se encuentra dentro del directorio de la misma, y el estado e instancias descargadas se guardaran en el directorio \texttt{data}. Para crear este directorio es necesario ejecutar:
	\begin{lstlisting}
	$ sbt stage
	\end{lstlisting}
	
	\subsubsection*{Configuración del crawler}
	Es posible configurar diversos parámetros del \textit{crawler} editando el archivo de configuración del mismo que se encuentra en \texttt{conf/crawler.conf} dentro de la carpeta de la aplicación.
	
	Los distinos parámetros que se pueden configurar son los siguientes:
	\begin{itemize}[noitemsep]
		\item \texttt{akka.http.client.user-agent-header}
		\item \texttt{akka.http.host-connection-pool.client.user-agent-header}: estos dos parámetros indican el User-Agent String usado por el crawler.
		\\
		\item \texttt{crawler.data-dir}: directorio donde se guardaran el estado y los datos descargados de la aplicación.
		\item \texttt{crawler.state-file}: fichero donde se guardará el estado del \textit{crawling}
		\item \texttt{crawler.download-dir}: directorio donde se guardarán las instancias descargadas.
		\item \texttt{crawler.cache-downloads}: indica si se han de guardar las instancias descargadas a disco o no.
		\item \texttt{crawler.index-downloads}: indica si se han de indexar las instancias descargadas en Elasticsearch o no.
		\item \texttt{crawler.throttle-speed}: tiempo que ha de pasar entre dos peticiones consecutivas de descarga de una instancia.
		\\
		\item \texttt{crawler.elasticsearch.host}: dirección del anfitrión de Elasticsearch.
		\item \texttt{crawler.elasticsearch.port}: puerto del anfitrión de Elasticsearch.
		\item \texttt{crawler.elasticsearch.index}: nombre del índice donde indexar las instancias descargadas.
		\item \texttt{crawler.elasticsearch.type}: nombre del tipo de los documentos en Elasticsearch.
		\item \texttt{crawler.elasticsearch.batch-size}: cada cuantos documentos hacer una petición de indexación a Easticsearch. Se realiza una petición en bloque con todos los documentos cuando se ha descargado el número suficiente de los mismos. Al parar el \textit{crawling} o cuando se hayan descargado todas las instancias se realizará una petición que incluya menos instancias.
	\end{itemize}
	
	\subsection{Proxy de acceso a Elasticsearch}
	La implementación de este proxy se encuentra en la carpeta \texttt{riws-proxy}. Es necesario tener instalado node.js para ejecutar esta aplicación.
	
	Primero es necesario descargar todas las dependencias de la aplicación, lo que se realiza con el comando:
	\begin{lstlisting}
	$ npm install
	\end{lstlisting}
	
	Para ejecutar el proxy se realiza de la misma manera que en el caso de la aplicación empaquetada (en este caso el contenido de esta carpeta y el de la aplicación empaquetada es el mismo). Es necesario introducir el comando:
	\begin{lstlisting}
	$ node server.js
	\end{lstlisting}
	
	Es posible indicar la localización de la instancia de Elasticsearch, en caso de que no se encuentre en la localización por defecto, mediante el uso de la variable de entorno \texttt{ELASTIC\_URL}. Por ejemplo:
	\begin{lstlisting}
	$ ELASTIC_URL=http://192.168.0.120:12345 npm run server
	\end{lstlisting}
	
	Si se desea utilizar un indice distinto es necesario editar el archivo \texttt{server.js} cambiando el nombre del indice de ``riws'' al nombre deseado.
	
	\subsection{Vista de búsqueda}
	La implementación de la vista de búsqueda se encuentra en el directorio \texttt{riws-search}. La implementación requiere que se tenga instalado node.js.
	
	Para instalar las dependencias necesarias hay que ejecutar el comando:
	\begin{lstlisting}
	$ npm install
	\end{lstlisting}
	
	Para ejecutar la aplicación de búsqueda en modo desarrollo es necesario ejecutar el comando:
	\begin{lstlisting}
	$ npm start
	\end{lstlisting}
	
	Si se quiere utilizar una dirección del proxy distinta de ``http://localhost:4000'' es necesario editar el archivo \texttt{src/App.js}, cambiando el valor de la constante ``host'' por la dirección deseada.
	
	Para construir la aplicación para que sea posible servirla de forma estática desde cualquier servidor HTTP hay que ejecutar el comando:
	\begin{lstlisting}
	$ npm run build
	\end{lstlisting}
	
	\section{Funcionalidades implementadas}
	Se presenta en esta sección un breve resumen de las funcionalidades implementadas.
	
	\begin{itemize}
		\item \textit{Crawler} para la página del Xestor de Proxectos. Este permite descargar las instancias de proyectos que se encuentra listadas en esta página, analizándolas, indexándolas y guardándolas en disco.
		\item Dicho \textit{crawler} se ha implementado de forma que se puedan descargar documentos de distintos sitio Web de manera concurrente, y ademas implementa una política de \textit{politeness}.
		\item Los documentos son indexados incluyendo los distintos campos, permitiendo la búsqueda de los mismo sin tener en cuenta si los términos de búsqueda incluyen los acentos correctos o no.
		\item Varios campos se indexan también de forma adecuada para que se puede realizar una búsqueda por facetas sobre algunos de sus campos.
		\item Se ha implementado un sencillo proxy de acceso a Elasticsearch para que la vista no tenga acceso directo al mismo.
		\item Se ha implementado una vista que permite buscar entre los documentos por varios de sus distintos campos, mostrando los resultados de la búsqueda y un resumen de cada una de las instancias. Estos resultados son mostrados de forma paginada.
		\item Es posible al mismo tiempo refinar la búsqueda usando un filtrado por facetas sobre los campo Titulación, Director y Tutor.
	\end{itemize}
	
	\section{Trabajo futuro}
	Como posible trabajo futuro se incluyen los siguiente puntos.
	
	\begin{itemize}
		\item Implementación de una política de \textit{freshness} en el \textit{crawler}. Ahora mismo no se vuelve a obtener la información de una instancia una vez se ha descargado.
		
		\item Aplicación para indexar los contenidos de los documentos que se han almacenado en disco.
		
		\item Mayor robustez en el proceso de iniciación del \textit{crawler}. Ahora mismo este falla entre otras cosas si el índice ya existe cuando se arranca sin ningún estado guardado. Sería posible que en este caso comprobase si el índice tiene la estructura deseada de los documentos y además que compruebe que documentos ya se encuentran indexados
		
		\item Indexación de los textos completos de los proyectos si se encuentran disponibles. Es posible descargar el archivo pdf con la memoria en alguno de estos proyectos, por lo que se podría indexar el contenido de los mismo.
		
		\item Opciones de búsqueda más avanzadas.
		
		\item \textit{Relevance feedback} en la búsqueda.
	\end{itemize}
	
	\begin{thebibliography}{9}
	 	\bibitem{akka-fsm} FSM - Akka Docummentation. \url{https://doc.akka.io/docs/akka/2.5.5/scala/fsm.html}
	 	
	 	\bibitem{erlang-fsm} Finite State Machines. The Erlang Design Principles. \url{http://erlang.org/documentation/doc-4.8.2/doc/design_principles/fsm.html}
	 	
	\end{thebibliography}
	
\end{document}