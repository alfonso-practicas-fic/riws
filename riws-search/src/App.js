import React, { Component } from 'react'
import extend from 'lodash/extend'
import { SearchkitManager,SearchkitProvider,
  SearchBox, RefinementListFilter, Pagination,
  HierarchicalMenuFilter, HitsStats, NoHits,
  ResetFilters, 
  ViewSwitcherHits, GroupedSelectedFilters,
  Layout, TopBar, LayoutBody, LayoutResults,
  ActionBar, ActionBarRow, SideBar } from 'searchkit'
import ShowMore from 'react-show-more'
import './index.css'

const host = "http://localhost:4000/"
const searchkit = new SearchkitManager(host)

const Alumno = (props) =>  {
  const {bemBlocks, alumno} = props
  return (<div className={bemBlocks.item("subtitle")}>Alumno: {alumno}</div>)
}

const Director = (props) =>  {
  const {bemBlocks, director} = props
  return (<div className={bemBlocks.item("subtitle")}>Director: {director}</div>)
}

const Tutor = (props) =>  {
  const {bemBlocks, tutor} = props
  return (<div className={bemBlocks.item("subtitle")}>Tutor: {tutor}</div>)
}

const InstanceHitsListItem = (props) => {
  const {bemBlocks, result} = props
  let url = "https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/" + result._id + "?t:ac=false"
  const source = extend({}, result._source, result.highlight)
  console.log(props)
  var i
  var asociaciones = []
  if ('alumno' in source) {
    for (i in source.alumno) {
      asociaciones.push(<Alumno alumno={source.alumno[i]} bemBlocks={bemBlocks} />)
    }
  } else {
    var nombre = source.author_name + " " + source.author_surname
    asociaciones.push(<Alumno alumno={nombre} bemBlocks={bemBlocks} />)
  }
  for (i in source.director) {
    asociaciones.push(<Director director={source.director[i]} bemBlocks={bemBlocks} />)
  }
  for (i in source.tutor) {
    asociaciones.push(<Tutor tutor={source.tutor[i]} bemBlocks={bemBlocks} />)
  }
  var texto
  if ('abstract' in source) {
    texto = source.abstract
  } else {
    texto = source.description
  }
  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <div className={bemBlocks.item("details")}>
        <a href={url} target="_blank"><h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.title}}></h2></a>
        {asociaciones}
        <ShowMore >{texto}</ShowMore>
      </div>
    </div>
  )
}

class App extends Component {
  render() {
    return (
      <SearchkitProvider searchkit={searchkit}>
        <Layout>
          <TopBar>
            <div className="my-logo">Xestor de proxectos</div>
            <SearchBox autofocus={true} searchOnChange={true}
                prefixQueryFields={["tutor", "tutor.folded", "director", "director.folded",
                                    "alumno", "alumno.folded", "description^2", "description.folded",
                                    "abstract^3", "abstract.folded", "title^5", "title.folded",
                                    "objective", "objective.folded", "material", "material.folded",
                                    "keywords^10", "keywords.folded", "methodology", "methodology.folded",
                                    "phases", "phases.folded"]}/>
          </TopBar>

        <LayoutBody>

          <SideBar>
            <HierarchicalMenuFilter fields={["degree.raw"]} title="Titulacion" id="categories"/>
            <RefinementListFilter id="directores" title="Director" field="director.raw" size={10}/>
            <RefinementListFilter id="tutores" title="Tutor" field="tutor.raw" size={10}/>
          </SideBar>
          <LayoutResults>
            <ActionBar>
              <ActionBarRow>
                <HitsStats translations={{
                  "hitstats.results_found":"{hitCount} results found"
                }}/>
              </ActionBarRow>

              <ActionBarRow>
                <GroupedSelectedFilters/>
                <ResetFilters/>
              </ActionBarRow>

            </ActionBar>
            <ViewSwitcherHits
                hitsPerPage={12} highlightFields={["title"]}
                sourceFilter={["title", "abstract", "author_name", "author_surname", 
                               "alumno", "director", "tutor", "description"]}
                hitComponents={[
                  {key:"list", title:"List", itemComponent:InstanceHitsListItem}
                ]}
                scrollTo="body"
            />
            <NoHits suggestionsField={"title"}/>
            <Pagination showNumbers={true}/>
          </LayoutResults>

          </LayoutBody>
        </Layout>
      </SearchkitProvider>
    );
  }
}

export default App;
