name := "riws-crawler"
version := "1.0"

scalaVersion := "2.12.4"

val elastic4sVersion = "5.6.0"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.6"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.6"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10" 
libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "2.0.0"
libraryDependencies +=  "com.sksamuel.elastic4s" %% "elastic4s-http-streams" % elastic4sVersion

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"

scalacOptions in GlobalScope in Compile := Seq("-unchecked", "-deprecation", "-feature")
scalacOptions in GlobalScope in Test := Seq("-unchecked", "-deprecation", "-feature")

enablePlugins(JavaAppPackaging)

/*
mappings in Universal ++= 
  ((resourceDirectory in Compile).value / "application.conf").get.map { f =>
    f -> "conf/crawler.conf"
  }
*/

bashScriptExtraDefines += """addJava "-Dconfig.file=${app_home}/../conf/crawler.conf""""
batScriptExtraDefines += """call :add_java "-Dconfig.file=%APP_HOME%\conf\crawler.conf""""

bashScriptExtraDefines += """addJava "-Dcrawler.home=${app_home}/../""""
batScriptExtraDefines += """call :add_java "-Dcrawler.home=%APP_HOME%\""""
