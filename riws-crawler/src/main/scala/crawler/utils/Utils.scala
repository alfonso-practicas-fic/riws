package crawler.utils

import java.nio.ByteBuffer
import java.nio.channels.AsynchronousFileChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

object Utils {
  def createParentDir(path: Path): Unit = {
    Files.createDirectories(path.getParent)
  }

  def writeToFile(
    filename: String,
    contents: String,
    createParent: Boolean = true)(implicit ec: ExecutionContext): Future[Int] = {
    val filePath = Paths.get(filename)
    if (createParent)
      createParentDir(filePath)
    val channel =
      AsynchronousFileChannel
        .open(
          filePath,
          StandardOpenOption.WRITE,
          StandardOpenOption.TRUNCATE_EXISTING,
          StandardOpenOption.CREATE)
    val writeJavaFuture = channel.write(ByteBuffer.wrap(contents.getBytes), 0l)
    Future { writeJavaFuture.get }
  }
}