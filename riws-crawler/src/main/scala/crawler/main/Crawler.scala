package crawler.main

import akka.Main.Terminator
import akka.actor.ActorSystem
import akka.actor.Props
import akka.stream.ActorMaterializer
import crawler.actors.MainActor
import crawler.config.DefaultConfig

object Crawler extends App {
    /* Make sure config is loaded to check for errors early */
  val config = DefaultConfig
  
  implicit val system = ActorSystem("prueba-main", config.realConfig)
  implicit val materializer = ActorMaterializer()
  
  val mainActor = system.actorOf(MainActor.mainActorProps, "main")
  val terminator = system.actorOf(Props(classOf[Terminator], mainActor), "terminator")
}