package crawler.indexing

import scala.concurrent.Promise

import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.http.bulk.BulkResponseItem
import com.sksamuel.elastic4s.streams.RequestBuilder
import com.sksamuel.elastic4s.streams.ResponseListener
import com.sksamuel.elastic4s.streams.SubscriberConfig

import akka.Done
import akka.NotUsed
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.actorRef2Scala
import akka.stream.scaladsl.Sink
import crawler.config.DefaultConfig

object IndexInstanceRequestBuilder {
  implicit val builder = new RequestBuilder[(Map[String, Any], Int)] {
    import com.sksamuel.elastic4s.http.ElasticDsl._

    val c = DefaultConfig

    def request(input: (Map[String, Any], Int)) = {
      val (instance, id) = input
      indexInto(c.elasticsearchIndex / c.elasticsearchType) id id.toString fields instance
    }
  }
}

class IndexerSink(
  client: HttpClient,
  manager: ActorRef,
  promise: Promise[Done])(implicit as: ActorSystem) {

  import IndexInstanceRequestBuilder.builder
  import com.sksamuel.elastic4s.streams.ReactiveElastic._
  import crawler.actors.MainActor._

  val crawlerConfig = DefaultConfig
  
  val responseListener = new ResponseListener[(Map[String, Any], Int)] {

    override def onAck(response: BulkResponseItem, input: (Map[String, Any], Int)) = {
      val (_, id) = input
      manager ! InstanceIndexedCorrectly(response, id)
    }

    override def onFailure(response: BulkResponseItem, input: (Map[String, Any], Int)) = {
      val (_, id) = input
      manager ! InstanceIndexedFailed(response, id)
    }

  }

  val subscriberConfig = SubscriberConfig(
    listener = responseListener,
    successFn = () => promise.success(Done),
    errorFn = { ex: Throwable => promise.failure(ex) },
    batchSize = crawlerConfig.elasticsearchBatchSize)

  val elasticIndexSubscriber =
    client.subscriber[(Map[String, Any], Int)](subscriberConfig)

  val sink = Sink.fromSubscriber(elasticIndexSubscriber)
}

object IndexerSink {
  def apply(
    client: HttpClient,
    manager: ActorRef,
    promise: Promise[Done])(implicit as: ActorSystem): Sink[(Map[String, Any], Int), NotUsed] =
    new IndexerSink(client, manager, promise)(as).sink
}