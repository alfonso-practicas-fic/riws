package crawler.indexing

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import crawler.config.DefaultConfig
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.index.CreateIndexResponse
import com.sksamuel.elastic4s.analyzers.StandardTokenizer
import com.sksamuel.elastic4s.analyzers.LowercaseTokenFilter
import com.sksamuel.elastic4s.analyzers.AsciiFoldingTokenFilter

object CreateMappings {
    def apply()(implicit ec: ExecutionContext): Future[CreateIndexResponse] = {
    import com.sksamuel.elastic4s.http.ElasticDsl._
    import com.sksamuel.elastic4s.mappings.FieldType._

    val config = DefaultConfig

    val client = HttpClient(ElasticsearchClientUri(
      config.elasticsearchHost,
      config.elasticsearchPort))
      
    val analyzer = customAnalyzer(
        "sin-acentos", 
        StandardTokenizer, 
        LowercaseTokenFilter, 
        AsciiFoldingTokenFilter)

    client.execute {
      createIndex(config.elasticsearchIndex).mappings(
        mapping(config.elasticsearchType) as (
          textField("abstract").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("alumno").fields(
            keywordField("raw"),
            textField("folded").analyzer("sin-acentos")
          ),
          textField("author_name").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("author_surname").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("degree").fields(
            keywordField("raw"),
            textField("folded").analyzer("sin-acentos")
          ),
          textField("department").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("description").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("director").fields(
            keywordField("raw"),
            textField("folded").analyzer("sin-acentos")
          ),
          keywordField("keywords").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          keywordField("login"),
          textField("material").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("notes").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("methodology").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("objective").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("phases").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("signature").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("title").fields(
            textField("folded").analyzer("sin-acentos")
          ),
          textField("tutor").fields(
            keywordField("raw"),
            textField("folded").analyzer("sin-acentos")
          ),
          textField("type").fields(
            keywordField("raw"),
            textField("folded").analyzer("sin-acentos")
          ))
      ).analysis(analyzer)
    }
      .map {
        response =>
          client.close()
          response
      }
  }

}