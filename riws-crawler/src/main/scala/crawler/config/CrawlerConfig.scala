package crawler.config

import scala.concurrent.duration.Duration

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory

trait CrawlerConfig {
  def config: Config

  private def fallbackConfig = ConfigFactory.parseResources("fallbacks.conf")
  def realConfig = ConfigFactory.load(config.withFallback(fallbackConfig))

  lazy val home: String = realConfig.getString("crawler.home")
  lazy val dataDir: String = realConfig.getString("crawler.data-dir")
  lazy val stateFile: String = realConfig.getString("crawler.state-file")
  lazy val downloadDir: String = realConfig.getString("crawler.download-dir")
  lazy val cacheDownloads: Boolean =
    realConfig.getBoolean("crawler.cache-downloads")
  lazy val indexDownloads: Boolean =
    realConfig.getBoolean("crawler.index-downloads")
  lazy val throttleSpeed =
    Duration
      .fromNanos(realConfig.getDuration("crawler.throttle-speed").toNanos())

  lazy val elasticsearchHost: String =
    realConfig.getString("crawler.elasticsearch.host")
  lazy val elasticsearchPort: Int =
    realConfig.getInt("crawler.elasticsearch.port")
  lazy val elasticsearchIndex: String =
    realConfig.getString("crawler.elasticsearch.index")
  lazy val elasticsearchType: String =
    realConfig.getString("crawler.elasticsearch.type")
  lazy val elasticsearchBatchSize: Int =
    realConfig.getInt("crawler.elasticsearch.batch-size")

  def checkConfig(): Unit = {
    val _ = List(home, dataDir, stateFile, downloadDir, cacheDownloads,
      indexDownloads, elasticsearchHost, elasticsearchPort, elasticsearchIndex,
      elasticsearchType)
    ()
  }
}

object DefaultConfig extends CrawlerConfig {
  val config = ConfigFactory.defaultApplication()

  checkConfig()
}