package crawler.fetching

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers
import akka.stream.Attributes
import akka.stream.ClosedShape
import akka.stream.FlowShape
import akka.stream.Inlet
import akka.stream.Outlet
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Broadcast
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Merge
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.SourceQueueWithComplete
import akka.stream.stage.GraphStage
import akka.stream.stage.GraphStageLogic
import akka.stream.stage.InHandler
import akka.stream.stage.OutHandler
import akka.stream.stage.StageLogging

class WithFollowRedirect[T](implicit system: ActorSystem)
  extends GraphStage[FlowShape[(HttpRequest, T), (HttpResponse, T)]] {

  val in = Inlet[(HttpRequest, T)]("WithFollowRedirect.in")
  val out = Outlet[(HttpResponse, T)]("WithFollowRedirect.out")

  override val shape = FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) with StageLogging {
      implicit lazy val fm = materializer

      var savedHeaders: Option[scala.collection.immutable.Seq[HttpHeader]] = None
      var queue: Option[SourceQueueWithComplete[(HttpRequest, T)]] = None
      var upstreamDone: Boolean = false
      var stagePulled: Boolean = false

      override def preStart() = {
        val httpFlow = Http().superPool[T]()

        val callback = getAsyncCallback[(HttpResponse, T)] {
          case (response, value) => handleResponse(response, value)
        }

        val sourceQueue = Source.queue[(HttpRequest, T)](10, OverflowStrategy.fail)

        val petitionGraph = RunnableGraph.fromGraph(
          GraphDSL.create(sourceQueue, httpFlow)(Keep.left) { implicit builder => (queue, httpFlow) =>
            import GraphDSL.Implicits._

            val bcast = builder.add(Broadcast[(Try[HttpResponse], T)](2))
            val merge = builder.add(Merge[(HttpRequest, T)](2))

            val redirections =
              Flow[(Try[HttpResponse], T)].collect(handleRedirection)

            val responses =
              Flow[(Try[HttpResponse], T)]
                .collect(collectResponse)
                .to(Sink.foreach(callback.invoke))

            queue ~> merge ~>   httpFlow   ~> bcast ~> responses
                     merge <~ redirections <~ bcast

            ClosedShape
          })

        queue = Some(petitionGraph.run())
      }

      def handleRedirection: PartialFunction[(Try[HttpResponse], T), (HttpRequest, T)] = {
        case (Success(response), value) if response.status == StatusCodes.Found =>
          response.discardEntityBytes()
          val locationUri: Uri = response.header[headers.Location].get.uri
          val req = HttpRequest(uri = locationUri, headers = savedHeaders.get)
          req -> value

      }

      def collectResponse: PartialFunction[(Try[HttpResponse], T), (HttpResponse, T)] = {
        case (Success(response), value) if response.status != StatusCodes.Found =>
          response -> value
        case (Failure(ex), _) => throw ex
      }

      def handleResponse(response: HttpResponse, value: T): Unit =
        push(out, (response, value))

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          stagePulled = false
          val (request, value) = grab(in)
          savedHeaders = Some(request.headers)
          queue.get.offer((request, value))
        }

        override def onUpstreamFinish(): Unit = {
          upstreamDone = true
          if (stagePulled)
            complete(out)
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          if (upstreamDone) {
            if (queue.isDefined) queue.get.complete else ()
            complete(out)
          } else {
            stagePulled = true
            pull(in)
          }
        }
      })
    }
}

object WithFollowRedirect {
  def apply[T]()(implicit system: ActorSystem): WithFollowRedirect[T] = new WithFollowRedirect
}