package crawler.fetching

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.apply
import akka.http.scaladsl.model.headers.Cookie
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import akka.stream.scaladsl.Flow

trait InstanceDownload[Ti, Th] {
  def sessionCookie: Cookie
  def getUri(id: Ti): Uri
  def outFlow: Flow[(HttpResponse, Ti), (Th, Ti), NotUsed]
  implicit val system: ActorSystem

  def getHttpRequest(i: Ti): HttpRequest =
    HttpRequest(uri = getUri(i), headers = List(sessionCookie))

  def getRequestPair(i: Ti): (HttpRequest, Ti) = getHttpRequest(i) -> i

  def flow: Flow[Ti, (Th, Ti), NotUsed] =
    Flow[Ti]
      .map(i => getRequestPair(i))
      .via(WithFollowRedirect())
      .filter(_._1.status == StatusCodes.OK)
      .via(outFlow)
}

private class IntStringInstanceDownload(val sessionCookie: Cookie)(implicit val system: ActorSystem, materializer: Materializer)
  extends InstanceDownload[Int, String] {

  import system.dispatcher

  override def getUri(id: Int): Uri =
    s"https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/$id?t:ac=false"

  override def outFlow: Flow[(HttpResponse, Int), (String, Int), NotUsed] =
    Flow[(HttpResponse, Int)]
      .mapAsync(1) {
        case (response, id) => Unmarshal(response).to[String] map (_ -> id)
      }
}

object InstanceDownload {
  def apply(sessionCookie: Cookie)(implicit system: ActorSystem, materializer: Materializer) =
    new IntStringInstanceDownload(sessionCookie).flow
}