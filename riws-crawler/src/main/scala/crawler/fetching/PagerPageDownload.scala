package crawler.fetching

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpCharsets
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.HttpMethods
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.MediaTypes
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers
import akka.http.scaladsl.model.headers.Cookie
import akka.http.scaladsl.settings.ConnectionPoolSettings
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import crawler.parsing.InstanceInfo
import crawler.parsing.InstanceInfoScrapper

case class PagerLink(linkId: String, url: String, zoneId: String)
case class PagerLinkZone(linkZone: List[PagerLink])
case class PagerResponse(content: String, inits: List[PagerLinkZone])

class PagerPageDownload(val sessionCookie: Cookie)(implicit val system: ActorSystem, materializer: ActorMaterializer) {

  import system.dispatcher
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  /* Classes and implicits for JSON response unmarshalling */
  import spray.json.DefaultJsonProtocol._

  private implicit val pagerLinkFormat = jsonFormat3(PagerLink)
  private implicit val pagerLinkZoneFormat = jsonFormat1(PagerLinkZone)
  private implicit val pagerResponseFormat = jsonFormat2(PagerResponse)

  val poolClientFlow = {
    val connectionSettings =
      ConnectionPoolSettings(system)
        .withMaxConnections(1)
        .withMaxOpenRequests(1)
    Http().superPool[Int](settings = connectionSettings)
  }

  private def requestUri(id: Int): String =
    s"https://xestor.fic.udc.es/proyectos/user/consultarinstancia.grid.pager/$id/grid?t:ac=false"

  private def createHttpRequest(uri: String): HttpRequest = {
    val requestedWithHeader =
      headers.RawHeader("X-Requested-With", "XMLHttpRequest")
    val entityData =
      "t%3Azoneid=grid" +
        "&t%3Aformid=instanciasForm" +
        "&t%3Aformcomponentid=user%2FConsultarInstancia%3Ainstanciasform"
    val entity = HttpEntity(
      MediaTypes.`application/x-www-form-urlencoded` withCharset HttpCharsets.`UTF-8`,
      entityData)
    HttpRequest(HttpMethods.POST, uri = uri, entity = entity,
      headers = List(sessionCookie, requestedWithHeader))
  }

  def collectResponse: PartialFunction[(Try[HttpResponse], Int), (HttpResponse, Int)] = {
    case (Success(response), value) if response.status != StatusCodes.Found =>
      response -> value
    case (Failure(ex), _) => throw ex
  }

  val downloadGraph: Flow[Int, (PagerResponse, Int), NotUsed] =
    Flow[Int].map(i => requestUri(i) -> i).map({
      case (uri, i) => createHttpRequest(uri) -> i
    })
      .via(poolClientFlow).map(collectResponse).mapAsync(1)({
        case (response, i) => Unmarshal(response).to[PagerResponse] map { _ -> i }
      })
}

object PagerPageDownload {
  private def findMaxPagerPage(linkZone: List[PagerLink]): Int = {
    def pagerPageLink = raw"https://xestor.fic.udc.es/proyectos/user/consultarinstancia.grid.pager/(\d+)/grid".r

    val pagerPages = for {
      pagerLink <- linkZone
      m <- pagerPageLink.findFirstMatchIn(pagerLink.url)
    } yield m.group(1).toInt
    pagerPages.max
  }

  def newDownloadGraph(sessionCookie: Cookie)(implicit
    system: ActorSystem,
    materializer: ActorMaterializer): Flow[Int, (PagerResponse, Int), NotUsed] =
    new PagerPageDownload(sessionCookie).downloadGraph

  def findMaxPagerPage(sessionCookie: Cookie)(implicit
    system: ActorSystem,
    materializer: ActorMaterializer): Future[Int] =
    Source.single(1)
      .via(newDownloadGraph(sessionCookie))
      .map({
        case (pagerResponse, _) => findMaxPagerPage(pagerResponse.inits(0).linkZone)
      })
      .runWith(Sink.head)

  def apply(sessionCookie: Cookie)(implicit
    system: ActorSystem,
    materializer: ActorMaterializer): Flow[Int, (List[InstanceInfo], Int), NotUsed] =
    newDownloadGraph(sessionCookie) map {
      case (response, i) => InstanceInfoScrapper.parsePagerResponse(response.content) -> i
    }

  def getInitialSeedData(sessionCookie: Cookie)(implicit
    system: ActorSystem,
    materializer: ActorMaterializer): Future[(List[InstanceInfo], Int)] =
    Source.single(1)
      .via(newDownloadGraph(sessionCookie))
      .map({
        case (response, _) =>
          InstanceInfoScrapper.parsePagerResponse(response.content) ->
            findMaxPagerPage(response.inits(0).linkZone)
      })
      .runWith(Sink.head)
}