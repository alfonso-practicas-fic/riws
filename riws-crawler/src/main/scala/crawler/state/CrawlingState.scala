package crawler.state

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.io.Source
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.Done
import crawler.config.DefaultConfig
import crawler.parsing.InstanceInfo
import spray.json.DefaultJsonProtocol
import spray.json.JsObject
import spray.json.RootJsonFormat
import spray.json.pimpAny
import spray.json.pimpString

object CrawlingStateProtocol extends DefaultJsonProtocol {
  implicit object CrawlingStateFormat extends RootJsonFormat[CrawlingState] {

    import crawler.parsing.InstanceInfoProtocol._
    import spray.json._

    /*
     * https://github.com/spray/spray-json/pull/209
     *
     * Allow to use convert Maps that use Int keys to/from JSON.
     */
    implicit def mapAnyValKeyFormat[K <: AnyVal: JsonFormat, V: JsonFormat] =
      new RootJsonFormat[Map[K, V]] {
        def write(m: Map[K, V]) = JsObject {
          m.map { field =>
            field._1.toJson match {
              case JsNumber(x) if x.isValidLong => x.toString -> field._2.toJson
              case x => throw new SerializationException(
                "Map key must be convertible to JsNumber, not '" + x + "'")
            }
          }
        }
        def read(value: JsValue) = value match {
          case x: JsObject => x.fields.map { field =>
            Try(JsNumber(BigDecimal(field._1))) match {
              case Success(k) => (k.convertTo[K], field._2.convertTo[V])
              case Failure(_) => deserializationError(
                "Expected Map key to be deserializable to JsNumber, but got '" +
                  field._1 + "'")
            }
          }(collection.breakOut)
          case x => deserializationError("Expected Map as JsObject, but got " + x)
        }
      }

    override def write(state: CrawlingState) = JsObject(
      "knownInstances" -> state.knownInstances.toJson,
      "downloadedInstances" -> state.downloadedInstances.toJson,
      "lastPagerPage" -> state.lastPagerPage.toJson,
      "downloadedPagerPages" -> state.downloadedPagerPages.toJson)

    override def read(value: JsValue) =
      value.asJsObject.getFields(
        "knownInstances",
        "downloadedInstances",
        "lastPagerPage",
        "downloadedPagerPages") match {

          case Seq(
            knownInstances: JsObject,
            downloadedInstances: JsObject,
            JsNumber(lastPagerPage),
            JsArray(downloadedPagerPages)) =>

            new CrawlingState(
              knownInstances.convertTo[Map[Int, InstanceInfo]],
              downloadedInstances.convertTo[Map[Int, InstanceInfo]],
              lastPagerPage.toInt,
              downloadedPagerPages.map(_.convertTo[Int]).toSet)

          case s =>
            throw new DeserializationException("ah ah ah!")
        }
  }
}

class CrawlingState(
  private[state] var knownInstances: Map[Int, InstanceInfo],
  private[state] var downloadedInstances: Map[Int, InstanceInfo],
  private[state] var lastPagerPage: Int,
  private[state] var downloadedPagerPages: Set[Int]) {

  override def toString: String =
    s"CrawlingState($knownInstances, $downloadedInstances, $lastPagerPage, $downloadedPagerPages"

  def save(filename: String)(implicit ec: ExecutionContext): Future[Done] = {
    import CrawlingStateProtocol._
    import crawler.utils.Utils._
    import spray.json._

    writeToFile(filename, this.toJson.prettyPrint) map { _ => Done }
  }

  def save()(implicit ec: ExecutionContext): Future[Done] = save(DefaultConfig.stateFile)

  def getInstancesFrontier: Seq[Int] =
    (knownInstances.keySet -- downloadedInstances.keySet).toList.sorted

  def getPagerPagesFrontier: Seq[Int] =
    (1 to lastPagerPage) filter (!downloadedPagerPages.contains(_))

  def putDownloadedInstances(ids: Seq[Int]): Unit =
    for {
      id <- ids
      instanceInfo <- knownInstances.get(id)
    } downloadedInstances += id -> instanceInfo

  def putDownloadedInstance(id: Int): Unit = putDownloadedInstances(Seq(id))

  def putDownloadedPagerPages(ids: Seq[Int]): Unit = downloadedPagerPages ++= ids

  def putDownloadedPagerPage(id: Int): Unit = downloadedPagerPages += id

  def putLastPagerPage(pagePage: Int): Unit = lastPagerPage = pagePage

  def putDiscoveredInstances(instances: Seq[InstanceInfo]): Unit =
    for (instance <- instances) knownInstances += instance.id -> instance

  def putDiscoveredInstance(instance: InstanceInfo): Unit =
    knownInstances += instance.id -> instance

  def getInstanceInfo(id: Int): InstanceInfo = knownInstances(id)
}

object CrawlingState {

  def apply(
    knownInstances: Map[Int, InstanceInfo],
    downloadedInstances: Map[Int, InstanceInfo],
    lastPagerPage: Int,
    downloadedPagerPages: Set[Int]) =
    new CrawlingState(knownInstances, downloadedInstances, lastPagerPage, downloadedPagerPages)

  def load(filename: String): CrawlingState = {
    import CrawlingStateProtocol._
    import spray.json._

    Source.fromFile(filename).mkString.parseJson.convertTo[CrawlingState]
  }

  def load(): CrawlingState = load(DefaultConfig.stateFile)
}

object InitialState {
  def apply() = new CrawlingState(Map(), Map(), 1, Set())
}
