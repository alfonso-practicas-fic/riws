package crawler.actors

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.Status
import akka.actor.Timers

object ConsoleActor {
  def consoleActorProps: Props = Props[ConsoleActor]

  /* Incomming messages */
  case class PromptUser(message: String)
  case class PromptUserForPassword(message: String)
  case class WaitForUserToInterrupt(message: String)
  case object CancelWaiting

  /* Outgoing messages */
  case class PromptUserResponse(response: String)
  case class PromptUserForPasswordResponse(response: String)
  case object UserInterruptedWait

  /* Private objects for Timers */
  private case object WatchKey
  private case class WatchTick(sender: ActorRef)

  private def isEnter(key: Int): Boolean = key == 10 || key == 13

  @tailrec private def shouldStopWaiting: Boolean =
    (System.in.available > 0) && (isEnter(System.in.read) || shouldStopWaiting)
}

class ConsoleActor extends Actor with Timers {
  import ConsoleActor._
  implicit val blockingDispatcher: ExecutionContext = 
    context.system.dispatchers.lookup("crawler.blocking-dispatcher")

  def receive = {
    case WaitForUserToInterrupt(msg) =>
      if (!timers.isTimerActive(WatchKey)) {
        println(msg)
        timers.startPeriodicTimer(WatchKey, WatchTick(sender()), 250.millis)
      }
    case WatchTick(sender) =>
      if (shouldStopWaiting) {
        timers.cancel(WatchKey)
        sender ! UserInterruptedWait
      }
    case CancelWaiting =>
      timers.cancel(WatchKey)
    case PromptUser(msg) =>
      print(msg)
      val senderRef = sender()
      Future { io.StdIn.readLine() } onComplete {
        case Success(response) => senderRef ! PromptUserResponse(response)
        case Failure(ex) => senderRef ! Status.Failure(ex)
      }
    case PromptUserForPassword(msg) =>
      print(msg)
      val senderRef = sender()
      Future { System.console.readPassword.mkString } onComplete {
        case Success(response) => senderRef ! PromptUserForPasswordResponse(response)
        case Failure(ex) => senderRef ! Status.Failure(ex)
      }
  }
}