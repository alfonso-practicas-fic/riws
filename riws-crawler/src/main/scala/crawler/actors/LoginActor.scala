package crawler.actors

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import akka.Done
import akka.NotUsed
import akka.actor.Actor
import akka.actor.Props
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.FormData
import akka.http.scaladsl.model.HttpCharsets
import akka.http.scaladsl.model.HttpMethods
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers
import akka.http.scaladsl.model.headers.Cookie
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.FlowShape
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import net.ruippeixotog.scalascraper.browser.JsoupBrowser

object LoginActor {
  def loginActorProps: Props = Props[LoginActor]

  /* Incomming messages. */
  final case class Login(username: String, password: String)

  /* Result messages. */
  sealed trait LoginResult
  final case class LoginSuccesful(sessionCookie: Cookie) extends LoginResult
  final case class LoginFailed(reason: Throwable) extends LoginResult

  case object BadCredentialsException extends Exception
}

class LoginActor extends Actor {
  import LoginActor._
  implicit val materializer = ActorMaterializer()

  private val successLocationUri: Uri =
    "https://xestor.fic.udc.es/proyectos/user/consultarinstancia/false"
  private val failLocationUri: Uri =
    "https://xestor.fic.udc.es/proyectos/user/login"
  private val startUri = "https://xestor.fic.udc.es/proyectos/"

  /**
   * Requests the login page.
   *
   *  Generates a request to the login page and results in a Future with the
   *  login credentials and the response to the request.
   */
  private def requestLoginPage(credentials: Login): Future[(Login, HttpResponse)] =
    Future.successful(credentials) zip
      Http(context.system).singleRequest(HttpRequest(uri = startUri))

  /**
   * Loads the body of the login page and gets the session cookie.
   *
   *  Gets the session cookie from the response headers and loads the body
   *  of the login page, resulting in a Future with the login credentials,
   *  the session cookie and the body of the login page.
   */
  private def loadLoginPage(input: (Login, HttpResponse)): Future[((Login, Cookie), String)] = {
    val (loginCredentials, response) = input
    if (response.status == StatusCodes.OK)
      response.header[headers.`Set-Cookie`] match {
        case Some(setCookie) =>
          Future.successful((loginCredentials, Cookie(setCookie.cookie.pair))) zip
            Unmarshal(response.entity).to[String]
        case None => Future.failed(new Exception("missing set cookie on login page"))
      }
    else
      Future.failed(new Exception("failed to load login page"))
  }

  /**
   * Parse the body of the login page and post login request.
   *
   *  Parse the body of the login page to get the data needed to request
   *  the login and generates a post requests to log in.
   *
   *  Results in a Future with the session cookie and the response to
   *  the login request.
   */
  private def parseLoginPageAndPostLogin(input: ((Login, Cookie), String)): Future[(Cookie, HttpResponse)] = {

    /* Get the needed data from the body string */
    def getActionUriAndFormDataValue(body: String): Option[(String, String)] = {
      import net.ruippeixotog.scalascraper.dsl.DSL._
      import net.ruippeixotog.scalascraper.dsl.DSL.Extract._

      val doc = JsoupBrowser().parseString(body)
      for {
        acitionUri <- doc >?> attr("action")("#loginForm")
        formDataValue <- doc >?> attr("value")("input[name=t:formdata]")
      } yield (acitionUri, formDataValue)
    }

    val ((Login(username, password), cookie), body) = input
    getActionUriAndFormDataValue(body) match {
      case Some((actionUri, formDataValue)) => {
        val entity = FormData(
          "t:formdata" -> formDataValue,
          "loginName" -> username, "password" -> password)
          .toEntity(HttpCharsets.`UTF-8`)
        val request = HttpRequest(
          method = HttpMethods.POST,
          uri = actionUri,
          entity = entity,
          headers = List(cookie))
        Future.successful(cookie) zip
          Http(context.system).singleRequest(request)
      }
      case None => Future.failed(new Exception("failed to parse login page"))
    }
  }

  /**
   * Checks for response to login request.
   *
   *  Checks if the login request was successful and request the redirected
   *  page if it was succesful, resulting in a Future of the session cookie
   *  and the response to this last request.
   */
  private def handleLoginResponse(input: (Cookie, HttpResponse)): Future[(Cookie, HttpResponse)] = {
    val (cookie, response) = input
    response.discardEntityBytes()
    if (response.status == StatusCodes.Found) {
      val locationHeader = response.header[headers.Location]
      locationHeader match {
        case Some(location) => location.uri match {
          case `successLocationUri` => {
            val request = HttpRequest(
              method = HttpMethods.GET,
              uri = successLocationUri,
              headers = List(cookie))
            Future.successful(cookie) zip
              Http(context.system).singleRequest(request)

          }
          case `failLocationUri` => Future.failed(BadCredentialsException)
          case _ => Future.failed(new Exception("unknown redirect uri during login"))
        }
        case None => Future.failed(new Exception("no Location header for Found response"))
      }
    } else
      Future.failed(new Exception("unknown login error"))
  }

  /**
   * Checks the response of the redirection after logging in.
   *
   *  Checks the status of the response of the redirect after logging in and
   *  consumes the body of the response, resulting in a Future with the session
   *  cookie.
   *
   */
  private def checkRedirectAfterLogin(input: (Cookie, HttpResponse)): Future[(Done, Cookie)] = {
    val (cookie, response) = input
    val discardedBytes = response.discardEntityBytes()
    if (response.status == StatusCodes.OK)
      discardedBytes.future() zip Future.successful(cookie)
    else
      Future.failed(new Exception("failed load of redirect after login"))
  }

  /** Graph with all the steps needed to login. */
  private val loginRequestGraph: Flow[Login, Cookie, NotUsed] =
    Flow.fromGraph(GraphDSL.create() {
      implicit builder =>
        import GraphDSL.Implicits._

        val input = builder.add(Flow[Login].mapAsync(1)(requestLoginPage))
        val output = builder.add(Flow[(Done, Cookie)].map({ x => x._2 }))

        input ~>
          Flow[(Login, HttpResponse)].mapAsync(1)(loadLoginPage) ~>
          Flow[((Login, Cookie), String)].mapAsync(1)(parseLoginPageAndPostLogin) ~>
          Flow[(Cookie, HttpResponse)].mapAsync(1)(handleLoginResponse) ~>
          Flow[(Cookie, HttpResponse)].mapAsync(1)(checkRedirectAfterLogin) ~>
          output

        FlowShape(input.in, output.out)
    })

  private def assembleLoginGraph(loginCredentials: Login): RunnableGraph[Future[Cookie]] =
    Source.single(loginCredentials).via(loginRequestGraph).toMat(Sink.head)(Keep.right)

  override def receive: Receive = {
    case message: Login => {
      implicit val blockingDispatcher: ExecutionContext =
        context.system.dispatchers.lookup("crawler.blocking-dispatcher")
      val senderRef = sender()
      assembleLoginGraph(message).run() onComplete {
        case Success(cookie) => {
          senderRef ! LoginSuccesful(cookie)
          context.stop(self)
        }
        case Failure(ex) => {
          senderRef ! LoginFailed(ex)
          context.stop(self)
        }
      }

    }
  }
}