package crawler.actors

import java.nio.file.Paths
import java.time.Duration
import java.time.LocalDateTime

import scala.concurrent.Future
import scala.concurrent.Promise
import scala.util.Failure
import scala.util.Success


import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.http.bulk.BulkResponseItem

import akka.Done
import akka.NotUsed
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.PoisonPill
import akka.actor.Props
import akka.http.scaladsl.model.headers.Cookie
import akka.stream.ActorMaterializer
import akka.stream.KillSwitches
import akka.stream.SinkShape
import akka.stream.ThrottleMode
import akka.stream.UniqueKillSwitch
import akka.stream.scaladsl.Broadcast
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import crawler.config.DefaultConfig
import crawler.fetching.InstanceDownload
import crawler.fetching.PagerPageDownload
import crawler.indexing.IndexerSink
import crawler.parsing.InstanceInfo
import crawler.parsing.InstancePageScraper
import crawler.state.CrawlingState

object MainActor {
  def mainActorProps(implicit materializer: ActorMaterializer): Props =
    Props(classOf[MainActor], materializer)

  private case object InitBootSequence
  private case class DownloadedPagerPage(instances: Seq[InstanceInfo], pagerPage: Int)
  private case object StateSaved

  case class InstanceIndexedCorrectly(response: BulkResponseItem, id: Int)
  case class InstanceIndexedFailed(response: BulkResponseItem, id: Int)

  case object GraphCompleted
  case class GraphFailed(ex: Throwable)
}

class MainActor(implicit val materializer: ActorMaterializer) extends Actor with ActorLogging {
  import BootActor._
  import ConsoleActor._
  import MainActor._

  val config = DefaultConfig

  val consoleActor = context.actorOf(consoleActorProps, "console")
  val bootActor = context.actorOf(bootActorProps, "boot-up")

  var crawlingState: Option[CrawlingState] = None
  var flowSwitch: Option[UniqueKillSwitch] = None
  var elasticClient: Option[HttpClient] = None
  var lastStateSave: Option[LocalDateTime] = None

  def discoveredInstancesSource(cookie: Cookie): Source[Int, NotUsed] = {
    import context.system

    Source(crawlingState.get.getPagerPagesFrontier.toList)
      .throttle(1, config.throttleSpeed, 1, ThrottleMode.shaping)
      .via(PagerPageDownload(cookie))
      .mapConcat {
        case (discoveredInstances, pagerPage) =>
          self ! DownloadedPagerPage(discoveredInstances, pagerPage)
          for (instance <- discoveredInstances) yield instance.id
      }
  }

  def initialInstanceListSource: Source[Int, NotUsed] =
    Source(crawlingState.get.getInstancesFrontier.toList)

  def instancesSource(cookie: Cookie): Source[Int, NotUsed] =
    initialInstanceListSource.concat(discoveredInstancesSource(cookie))

  def writeToFileSink: Sink[(String, Int), Future[Done]] = {
    import crawler.utils.Utils._
    def filename(id: Int): String =
      Paths.get(config.downloadDir).resolve(s"$id.html").toString

    implicit val ec = context.system.dispatchers.lookup("crawler.blocking-dispatcher")

    Flow[(String, Int)].mapAsync(4) {
      case (contents, id) => writeToFile(filename(id), contents)
    }
      .toMat(Sink.ignore)(Keep.right)
  }

  def indexInstanceSink: Sink[(String, Int), Future[Done]] = {
    import context.system

    val client = elasticClient.get
    val promise = Promise[Done]

    Flow[(String, Int)]
      .map { case (instance, id) => InstancePageScraper(instance) -> id }
      .to(IndexerSink(client, self, promise))
      .mapMaterializedValue(_ => promise.future)
  }

  def bothSinks: Sink[(String, Int), Future[Done]] = {
    import context.dispatcher

    val graph = Sink.fromGraph(
      GraphDSL.create(writeToFileSink, indexInstanceSink)(Keep.both) { implicit builder => (disk, index) =>
        import GraphDSL.Implicits._

        val bcast = builder.add(Broadcast[(String, Int)](2))

        bcast ~> disk
        bcast ~> index

        SinkShape(bcast.in)
      })
    graph.mapMaterializedValue({
      case (future1, future2) =>
        for {
          result1 <- future1
          result2 <- future2
        } yield Done
    })
  }

  def crawlingGraph(cookie: Cookie): RunnableGraph[(UniqueKillSwitch, Future[Done])] = {
    import context.system

    val downloadedInstancesSource = instancesSource(cookie)
      .throttle(1, config.throttleSpeed, 1, ThrottleMode.shaping)
      .viaMat(KillSwitches.single)(Keep.right)
      .via(InstanceDownload(cookie))

    val graphSink = (config.indexDownloads, config.cacheDownloads) match {
      case (true, true) => bothSinks
      case (true, false) => indexInstanceSink
      case (false, true) => writeToFileSink
      case (false, false) =>
        log.warning("Downloaded instances are being ignored. Not indexing or saving to disk. Check config")
        Sink.ignore
    }

    downloadedInstancesSource.toMat(graphSink)(Keep.both)
  }

  def saveState() = {
    implicit val ec = context.system.dispatchers.lookup("crawler.blocking-dispatcher")
    for {
      state <- crawlingState
    } yield state.save().map { _ => self ! StateSaved }
  }

  def checkSaveState() = {
    val now = LocalDateTime.now
    val timePassed = for {
      saveTime <- lastStateSave
    } yield Duration.between(saveTime, now)
    timePassed match {
      case Some(duration) if duration.abs().getSeconds > 10 => saveState()
      case Some(duration) => None
      case None => saveState() // Save if we have never saved. Shouldn't happen.
    }
  }

  override def preStart() {
    self ! InitBootSequence
  }

  override def receive = {

    case InitBootSequence =>
      bootActor ! BootUp(consoleActor)

    case BootUpUserCanceled =>
      log.info("Crawling cancelled")
      context stop self

    case BootUpFailed(reason) =>
      log.error(reason, "Boot up failed")
      context stop self

    case BootUpDone(cookie, state) =>
      bootActor ! Die
      crawlingState = Some(state)
      saveState()
      elasticClient =
        Some(HttpClient(ElasticsearchClientUri(
          config.elasticsearchHost, config.elasticsearchPort)))

      val (switch, future) = crawlingGraph(cookie).run()
      flowSwitch = Some(switch)
      import context.dispatcher
      future onComplete {
        case Success(Done) => self ! GraphCompleted
        case Failure(reason) => self ! GraphFailed(reason)
      }

      consoleActor ! WaitForUserToInterrupt("Crawling started. Press ENTER to finish it.")

    case UserInterruptedWait =>
      log.info("Aborting crawler")
      flowSwitch map (_.shutdown())

    case DownloadedPagerPage(instances, pagerPage) =>
      crawlingState.map(_.putDownloadedPagerPage(pagerPage))
      crawlingState.map(_.putDiscoveredInstances(instances))
      checkSaveState()
      log.info(s"Downloaded pagerPage $pagerPage")

    case InstanceIndexedCorrectly(_, id) =>
      crawlingState.map(_.putDownloadedInstance(id))
      checkSaveState()
      log.info("Instance indexed: {}", id)

    case InstanceIndexedFailed(_, id) =>
      log.info("Instance indexing failed: {}", id)

    case StateSaved => lastStateSave = Some(LocalDateTime.now)

    case GraphCompleted =>
      log.info("Graph completed, cleaning up")
      elasticClient map (_.close)
      saveState() match {
        case Some(future) =>
          import context.dispatcher
          future.map { _ => self ! PoisonPill }
        case None => Future.successful(Unit)
      }

    case GraphFailed(ex) =>
      log.error(ex, "Crawling failed")
      elasticClient map (_.close)
      context stop self

    case x =>
      log.error("Unhandled message: {}", x)
  }
}