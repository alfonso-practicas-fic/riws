package crawler.actors

import java.nio.file.Files
import java.nio.file.Paths

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import com.sksamuel.elastic4s.http.index.CreateIndexResponse

import akka.actor.ActorRef
import akka.actor.FSM
import akka.actor.Props
import akka.http.scaladsl.model.headers.Cookie
import akka.stream.ActorMaterializer
import crawler.config.DefaultConfig
import crawler.fetching.PagerPageDownload
import crawler.indexing.CreateMappings
import crawler.parsing.InstanceInfo
import crawler.state.CrawlingState
import crawler.state.InitialState

/**
 * Actor to execute boot up sequence. The sequence is:
 *
 *   - Prompt user for credentials.
 *   - Login into xestor.
 *   - Load previous state if it exists.
 *   - Load initial state if previous state doesn't exists.
 *   - Create mappings if needed
 */

sealed trait State
case object Initial extends State
case object AskingForUsername extends State
case object AskingForUsernameAgain extends State
case object AskingForPassword extends State
case object Authenticating extends State
case object WarmBoot extends State
case object WarmBootStateLoaded extends State
case object WarmBootMaxPagerPageDiscovered extends State
case object ColdBoot extends State
case object ColdBootInitialSeedDataLoaded extends State
case object ColdBootIndexCreated extends State
case object BootDone extends State

sealed trait Data
case object Uninitialized extends Data
abstract class Initiated(val initiator: ActorRef) extends Data
final case class AskingForUsernameData(
  override val initiator: ActorRef,
  console: ActorRef) extends Initiated(initiator)
final case class AskingForPasswordData(
  override val initiator: ActorRef,
  username: String,
  console: ActorRef) extends Initiated(initiator)
final case class AuthenticatingData(
  override val initiator: ActorRef,
  console: ActorRef) extends Initiated(initiator)
final case class WarmBootData(
  override val initiator: ActorRef,
  sessionCookie: Cookie) extends Initiated(initiator)
final case class WarmBootStateLoadedData(
  override val initiator: ActorRef,
  sessionCookie: Cookie,
  state: CrawlingState) extends Initiated(initiator)
final case class WarmBootMaxPagerPageDiscoveredData(
  override val initiator: ActorRef,
  sessionCookie: Cookie,
  maxPagerPage: Int) extends Initiated(initiator)
final case class ColdBootData(
  override val initiator: ActorRef,
  sessionCookie: Cookie) extends Initiated(initiator)
final case class ColdBootInitialSeedData(
  override val initiator: ActorRef,
  sessionCookie: Cookie,
  initialState: CrawlingState) extends Initiated(initiator)

object BootActor {
  def bootActorProps(implicit am: ActorMaterializer): Props = Props(new BootActor)

  case class BootUp(console: ActorRef)
  case object Die

  final case class BootUpDone(sessionCookie: Cookie, state: CrawlingState)
  final case object BootUpUserCanceled
  final case class BootUpFailed(reason: Throwable)

  private final case class InitialSeedData(instancesInfo: List[InstanceInfo], maxPagerPage: Int)
  private final case class IndexCreated(createIndexResponse: CreateIndexResponse)
  private final case class MaxPagerPage(maxPagerPage: Int)
  private final case class LoadedCrawlingState(state: CrawlingState)
}

/**
 * Actor to execute boot up sequence. The sequence is:
 *
 *   - Prompt user for credentials.
 *   - Login into xestor.
 *   - If previous status exists:
 *     + Load previous state
 *     + Check for last pager page
 *   - If previous state doesn't exist:
 *     + Load initial seed data, i.e. last pager page and initial instance info
 */
class BootActor(implicit val am: ActorMaterializer) extends FSM[State, Data] {
  import BootActor._
  import ConsoleActor._
  import LoginActor._

  startWith(Initial, Uninitialized)

  when(Initial) {
    case Event(BootUp(console), _) =>
      console ! PromptUser("Usuario: ")
      goto(AskingForUsername) using AskingForUsernameData(sender(), console)
  }

  when(AskingForUsername) {
    case Event(PromptUserResponse(response), d @ AskingForUsernameData(initiator, console)) if response == "" =>
      val msg = """Usuario estaba vacio. Introduce un usuario vacio de nuevo para salir.
                  |Usuario: """.stripMargin
      console ! PromptUser(msg)
      goto(AskingForUsernameAgain) using d

    case Event(PromptUserResponse(response), AskingForUsernameData(initiator, console)) =>
      console ! PromptUserForPassword("Contraseña: ")
      goto(AskingForPassword) using AskingForPasswordData(initiator, response, console)
  }

  when(AskingForUsernameAgain) {
    case Event(PromptUserResponse(response), d @ AskingForUsernameData(initiator, console)) if response == "" =>
      initiator ! BootUpUserCanceled
      goto(BootDone) using Uninitialized

    case Event(PromptUserResponse(response), AskingForUsernameData(initiator, console)) =>
      console ! PromptUserForPassword("Contraseña: ")
      goto(AskingForPassword) using AskingForPasswordData(initiator, response, console)
  }

  when(AskingForPassword) {
    case Event(PromptUserForPasswordResponse(response), AskingForPasswordData(initiator, username, console)) =>
      val loginActor = context.actorOf(loginActorProps)
      loginActor ! Login(username, response)
      goto(Authenticating) using AuthenticatingData(initiator, console)
  }

  when(Authenticating) {
    case Event(LoginSuccesful(cookie), AuthenticatingData(initiator, _)) =>
      import context.dispatcher
      import context.system
      if (isTherePreviousState()) {
        PagerPageDownload.findMaxPagerPage(cookie) map {
          case maxPagerPage => self ! MaxPagerPage(maxPagerPage)
        }
        Future { CrawlingState.load() } map {
          case crawlingState => self ! LoadedCrawlingState(crawlingState)
        }
        goto(WarmBoot) using WarmBootData(initiator, cookie)
      } else {
        PagerPageDownload.getInitialSeedData(cookie) map {
          case (instancesInfo, maxPagerPage) =>
            self ! InitialSeedData(instancesInfo, maxPagerPage)
        }
        CreateMappings.apply.onComplete {
          case Success(response) => self ! IndexCreated(response)
          case Failure(reason) => self ! Failure(reason)
        }
        goto(ColdBoot) using ColdBootData(initiator, cookie)
      }
    case Event(LoginFailed(reason), AuthenticatingData(initiator, console)) =>
      reason match {
        case BadCredentialsException =>
          val msg = """Usuario/contraseña incorrectos. Introduce usuario vacio dos veces para salir.
                  |Usuario: """.stripMargin
          console ! PromptUser(msg)
          goto(AskingForUsername) using AskingForUsernameData(initiator, console)
        case _ =>
          self ! Failure(reason)
          stay()
      }
  }

  when(WarmBoot) {
    case Event(MaxPagerPage(maxPagerPage), WarmBootData(initiator, cookie)) =>
      goto(WarmBootMaxPagerPageDiscovered) using WarmBootMaxPagerPageDiscoveredData(initiator, cookie, maxPagerPage)

    case Event(LoadedCrawlingState(state), WarmBootData(initiator, cookie)) =>
      goto(WarmBootStateLoaded) using WarmBootStateLoadedData(initiator, cookie, state)
  }

  when(WarmBootMaxPagerPageDiscovered) {
    case Event(LoadedCrawlingState(state), WarmBootMaxPagerPageDiscoveredData(initiator, cookie, maxPagerPage)) =>
      state.putLastPagerPage(maxPagerPage)
      initiator ! BootUpDone(cookie, state)
      goto(BootDone) using Uninitialized
  }

  when(WarmBootStateLoaded) {
    case Event(MaxPagerPage(maxPagerPage), WarmBootStateLoadedData(initiator, cookie, state)) =>
      state.putLastPagerPage(maxPagerPage)
      initiator ! BootUpDone(cookie, state)
      goto(BootDone) using Uninitialized
  }

  when(ColdBoot) {
    case Event(InitialSeedData(instancesInfo, maxPagerPage), ColdBootData(initiator, cookie)) =>
      val crawlingState = InitialState()
      crawlingState.putLastPagerPage(maxPagerPage)
      crawlingState.putDiscoveredInstances(instancesInfo)
      crawlingState.putDownloadedPagerPage(1)
      goto(ColdBootInitialSeedDataLoaded) using ColdBootInitialSeedData(initiator, cookie, crawlingState)
    case Event(IndexCreated(createIndexResponse), data @ ColdBootData(initiator, cookie)) =>
      if (createIndexResponse.acknowledged)
        goto(ColdBootIndexCreated) using data
      else {
        self ! Failure(new Exception("index create failed"))
        stay()
      }
  }

  when(ColdBootInitialSeedDataLoaded) {
    case Event(IndexCreated(createIndexResponse), ColdBootInitialSeedData(initiator, cookie, crawlingState)) =>
      initiator ! BootUpDone(cookie, crawlingState)
      goto(BootDone) using Uninitialized
  }

  when(ColdBootIndexCreated) {
    case Event(InitialSeedData(instancesInfo, maxPagerPage), ColdBootData(initiator, cookie)) =>
      val crawlingState = InitialState()
      crawlingState.putLastPagerPage(maxPagerPage)
      crawlingState.putDiscoveredInstances(instancesInfo)
      crawlingState.putDownloadedPagerPage(1)
      initiator ! BootUpDone(cookie, crawlingState)
      goto(BootDone) using Uninitialized
  }

  when(BootDone) {
    case Event(Die, _) => stop()
  }

  whenUnhandled {
    case Event(Failure(reason), Uninitialized) => throw reason
    case Event(Failure(reason), initiated: Initiated) =>
      initiated.initiator ! BootUpFailed(reason)
      goto(BootDone) using Uninitialized
  }

  def isTherePreviousState(): Boolean =
    Files.exists(Paths.get(DefaultConfig.stateFile))

  initialize()
}
