package crawler.parsing

case class InstanceInfo(id: Int, title: String, pupil: String,
    state: String, url: String)

object InstanceInfoProtocol {
  import spray.json.DefaultJsonProtocol._
  implicit val instanceInfoFormat = jsonFormat5(InstanceInfo)
}

object InstanceInfoScrapper {
    def parsePagerResponse(content: String): List[InstanceInfo] = {
    def parseId(text: String): Int = {
      val idMatch =
        raw"https://xestor.fic.udc.es/proyectos/user/consultarinstancia.ver/(\d+)\?t:ac=false".r
      text match {
        case idMatch(id) => id.toInt
        case _ => -1
      }
    }
    import net.ruippeixotog.scalascraper.browser.JsoupBrowser
    import net.ruippeixotog.scalascraper.dsl.DSL._
    import net.ruippeixotog.scalascraper.dsl.DSL.Extract._

    val doc = JsoupBrowser().parseString(content)
    val table = doc >> element("#gridDatos")
    for {
      row <- table >> elementList("tr")
      if (row >?> element("td")).isDefined
      title = row >> text(".tituloPro")
      pupil = row >> text(".loginAlumno")
      state = row >> text(".estado")
      url = row >> attr("href")(".botones a")
      id = parseId(url)
      if id > 1
    } yield InstanceInfo(id, title, pupil, state, url)
  }

}