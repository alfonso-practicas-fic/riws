package crawler.parsing

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.model.Document

object InstancePageScraper {

  def getFieldValue(doc: Document, labelValue: String): Option[String] = {
    import net.ruippeixotog.scalascraper.dsl.DSL._
    import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
    import net.ruippeixotog.scalascraper.model._

    val labelElements = doc >?> elements(s"""label[for="$labelValue"]""")
    val rawString = for {
      labelQuery <- labelElements
      labelList = labelQuery.toList
      if labelList.size == 1
      label = labelList.head
      parent <- label.parent
      value <- parent >?> text(".datos")
      if value != "-" && value != ""
    } yield value
    
    /* Remove non printable chars */
    rawString.map(_.filter(_ >= 0x20))
  }
  
  def getAssociations(doc: Document): Map[String, List[String]] = {
    import net.ruippeixotog.scalascraper.dsl.DSL._
    import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
    import net.ruippeixotog.scalascraper.model._

    def reorderName(name: String): String = name.split(", ") match {
      case l if l.size == 2 => List(l(1), l(0)).mkString(" ")
      case _ => name
    }
    
    val tables = doc >> elements("table")
    
    if (tables.size != 1)
      Map.empty
    else {
      val associationsPairs = 
        (tables >> element("tbody") >> elements("tr")).toList >> (text(".relacion"), text(".usuario"))
      associationsPairs.map {
          case (relation, name) => relation.toLowerCase -> reorderName(name)
        }.groupBy(_._1).mapValues(_.map(_._2))
    }
    
  }

  def apply(instance: String): Map[String, Any] = {
    val doc = JsoupBrowser().parseString(instance)

    val fields = Map("titulo" -> "title", "titulacion" -> "degree",
      "tipo" -> "type", "objetivo" -> "objective", "descripcion" -> "description",
      "material" -> "material", "metodologia" -> "methodology", "fases" -> "phases",
      "departamento" -> "department", "estado" -> "state", "loginAlumno" -> "login",
      "nombreAlumno" -> "author_name", "apellidosAlumno" -> "author_surname",
      "anotaciones" -> "notes", "resumen" -> "abstract", "signature" -> "signature",
      "palabrasClave" -> "keywords")

    val singleValueFields = for {
      (fieldLabelValue, fieldName) <- fields
      value <- getFieldValue(doc, fieldLabelValue)
    } yield fieldName -> value
    
    singleValueFields ++ getAssociations(doc)
  }
}