var express = require("express");
var SearchkitExpress = require("searchkit-express")
var bodyParser = require("body-parser")
var cors = require('cors')

var app = express()
app.use(bodyParser.json())
app.use("/", cors({
      origin:"*",
      maxAge:20*24*60*60 //20 days like elastic
    }))
    
SearchkitExpress({
  // For HTTP basic auth use format: "http://username:password@localhost:9200" in host parameter
  host:process.env.ELASTIC_URL || "http://localhost:9200", 
  index:'riws',
  queryProcessor:function(query, req, res){
    //do neccessery permissions, prefilters to query object
    //then return it
    return query
  }
 }, app)

app.listen(4000,  function () {
      console.log('server running at localhost:4000, go refresh and see magic');
    })  
